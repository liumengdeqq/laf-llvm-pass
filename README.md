# LAF LLVM Passes
This is a collection of LLVM passes which try to increase the coverage of
feedback driven fuzzers such as AFL and libFuzzer. You can read more about it
[here](https://lafintel.wordpress.com/2016/08/15/circumventing-fuzzing-roadblocks-with-compiler-transformations/)

## Requirements
- Clang 3.8.0. Should work with newer Clang versions as well, but we have not
verified this.

## Installation
1. Clone or download the files.
2. Copy the *.so.cc files from `src/` and the afl.patch file to the
`$AFL_PATH/llvm_mode`
3. Change to the `$AFL_PATH/llvm_mode` directory and apply the patch using
`patch < afl.patch`
4. Compile AFL and the llvm mode as usual.

## Usage
By default the passes will not run when you compile programs using
`afl-clang-fast`. Hence, you can use AFL as usual. To enable the passes you must
set environment variables before you compile the target project.

- `export LAF_SPLIT_SWITCHES=1`
    Enables the split-switches pass.
- `export LAF_TRANSFORM_COMPARES=1`
    Enables the transform-compares pass (strcmp, memcmp).
- `export LAF_SPLIT_COMPARES=1`
    Enables the split-compares pass. By default it will split all compares with
    a bit width <= 64 bits. You can change this behaviour by setting
    `export LAF_SPLIT_COMPARES_BITW=<bit_width>`.
    
## Tips
Do not use only binaries compiled with these passes in your fuzzing session.
Mix instances of normal AFL instrumentation and binaries compiled with these
passes. We would suggest to try the following setup (for 6 instances):
- Normal AFL instrumentation:
    - One Master, three slaves
- LAF instrumentation:
    - One Master, one slave

Remember: Everything depends on the target. So be sure to try different
setups! This is just a starting point.